import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POST_INFO_SUCCESS = 'FETCH_POST_INFO_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const CREATE_POSTS_SUCCESS = 'CREATE_POSTS_SUCCESS';
export const DEL_POST = 'DEL_POST';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostInfoSuccess = post => ({type: FETCH_POST_INFO_SUCCESS, post});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});
export const delaPost = id => ({type: DEL_POST, id});

export const createPostsSuccess = () => ({type: CREATE_POSTS_SUCCESS});

export const fetchPosts = () => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts');
      dispatch(fetchPostsSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure());
      NotificationManager.error('Could not fetch products');
    }
  }
};

export const fetchPostInfo = (id) => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts/' + id);
      dispatch(fetchPostInfoSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure());
      NotificationManager.error('Could not fetch products');
    }
  }
};

export const createPost = postData => {
  return async dispatch => {
    await axiosApi.post('/posts', postData);
    dispatch(createPostsSuccess());
  };
};

// export const delPost = (id) => {
//   return async () => {
//     await axiosApi.delete('posts/' + id + 'json');
//   };
// };

export const delPost = (id) => {
  return async dispatch => {
    try {
      await axiosApi.delete('posts/' + id);
      dispatch(fetchPosts());
    } catch (error) {
      console.log(error);
    }
  };
};