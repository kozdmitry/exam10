import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_COMMENT_REQUEST = 'FETCH_COMMENT_REQUEST';
export const FETCH_COMMENT_SUCCESS = 'FETCH_COMMENT_SUCCESS';
export const FETCH_COMMENT_INFO_SUCCESS = 'FETCH_COMMENT_INFO_SUCCESS';
export const FETCH_COMMENT_FAILURE = 'FETCH_COMMENT_FAILURE';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const DEL_COMMENT = 'DEL_POST';

export const fetchCommentRequest = () => ({type: FETCH_COMMENT_REQUEST});
export const fetchCommentSuccess = posts => ({type: FETCH_COMMENT_SUCCESS, posts});
export const fetchCommentInfoSuccess = post => ({type: FETCH_COMMENT_INFO_SUCCESS, post});
export const fetchCommentFailure = () => ({type: FETCH_COMMENT_FAILURE});
export const delaPost = id => ({type: DEL_COMMENT, id});

export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});

export const fetchComment = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchCommentRequest());
            const response = await axiosApi.get('/comments?news_id=' + id);
            dispatch(fetchCommentSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentFailure());
            NotificationManager.error('Could not fetch products');
        }
    }
};


export const createComment = commentsData => {
    return async dispatch => {
        await axiosApi.post('/comments', commentsData);
        dispatch(createCommentSuccess());
    };
};

export const delComment = (id) => {
    return async dispatch => {
        try {
            await axiosApi.delete('comments/' + id);
            dispatch(fetchComment());
        } catch (error) {
            console.log(error);
        }
    };
};