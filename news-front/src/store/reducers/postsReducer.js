import {
  FETCH_POST_INFO_SUCCESS,
  FETCH_POSTS_FAILURE,
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
  posts: [],
  post: [],
  postsLoading: false,
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_REQUEST:
      return {...state, postsLoading: true};
    case FETCH_POSTS_SUCCESS:
      return {...state, postsLoading: false, posts: action.posts};
    case FETCH_POST_INFO_SUCCESS:
      return {...state, postsLoading: false, post: action.post};
    case FETCH_POSTS_FAILURE:
      return {...state, postsLoading: false};
    default:
      return state;
  }
};

export default postsReducer;