import React, {useEffect} from 'react';
import {Card, CardActions, CardContent, CardHeader, CardMedia, IconButton, Paper} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import PostInfo from "../../components/PostInfo/PostInfo";
import {useDispatch, useSelector} from "react-redux";
import {delPost, fetchPostInfo} from "../../store/actions/postsActions";

const useStyles = makeStyles((theme) => ({
  itemMessage: {
    border: "2px solid grey",
    margin: "10px",
    padding: "5px",
  },
  cover: {
    width: '150px',
    height: '150px',
    overflow: 'hidden',
    borderRadius: '4px'
  },
}));


const PostItem = (props) => {
  const dispatch = useDispatch();
  const post = useSelector(state => state.posts.post);

  const oneInfo = (id) => {
    dispatch(fetchPostInfo(id));
  };
  useEffect(() => {
    dispatch(fetchPostInfo(props.id))
  }, [dispatch]);

  const classes = useStyles();
  return (
      <>
        <Paper className={classes.itemMessage}>
          <h4>Author: {props.title}</h4>
          <p>Message: {props.content}</p>
          <p>Date: {props.date}</p>
          <CardMedia
              className={classes.cover}
              image={'http://localhost:8000/uploads/' + props.image}
          />
          <IconButton onClick={() => oneInfo(props.id)} component={Link} to={'/posts/' + props.id}>
            READ FULL POST >>
          </IconButton>
          <button onClick={props.del}>del</button>
        </Paper>
    {/*<Grid item xs sm md={6} lg={4}>*/}
    {/*  <Card>*/}
    {/*    <CardHeader title={props.title}/>*/}
    {/*    <CardMedia*/}
    {/*        className={classes.cover}*/}
    {/*        image={'http://localhost:8000/uploads/' + props.image}*/}
    {/*    />*/}
    {/*    <CardContent>*/}
    {/*      <strong style={{marginLeft: '10px'}}>*/}
    {/*        {props.content}*/}
    {/*      </strong>*/}
    {/*    </CardContent>*/}
    {/*    <CardActions>*/}
    {/*      <p>Date: {props.date}</p>*/}
    {/*      <IconButton onClick={() => oneInfo(props.id)} component={Link} to={'/posts/' + props.id}>*/}
    {/*        READ FULL POST >>*/}

    {/*      </IconButton>*/}
    {/*      <PostInfo*/}
    {/*          id={props.id}*/}
    {/*          key={props.id}*/}
    {/*      />*/}
    {/*    /!*  <IconButton onClick={del(props.id)}>*!/*/}

    {/*    /!*  Delete*!/*/}
    {/*    /!*</IconButton>*!/*/}
    {/*      <button onClick={props.del}>del</button>*/}
    {/*    </CardActions>*/}
    {/*  </Card>*/}
    {/*</Grid>*/}
        </>
  );
};

export default PostItem;