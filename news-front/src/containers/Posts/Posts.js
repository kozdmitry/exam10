import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import {delPost, fetchPosts} from "../../store/actions/postsActions";
import PostItem from "./PostItem";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
  progress: {
    height: 200
  }
}));

const Posts = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const posts = useSelector(state => state.posts.posts);
  const loading = useSelector(state => state.posts.posts);

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);


  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h4">Posts</Typography>
        </Grid>
        <Grid item>
          <Button color="primary" component={Link} to="/posts/new">Add new posts</Button>
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        {posts.map(post => (
          <PostItem
            key={post.id}
            id={post.id}
            title={post.title}
            content={post.content}
            image={post.image}
            date={post.date}
            del={delPost(post.id)}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Posts;