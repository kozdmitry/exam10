import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia, IconButton, Paper} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {createComment, fetchComment} from "../../store/actions/commentActions";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
}));

const PostInfo = (props) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const comments = useSelector(state => state.comments.comments);
    const classes = useStyles();
    console.log(comments);


    const [state, setState] = useState({
        news_id: props.id,
        name: '',
        comment: '',
    });
    const data = {
        post,
        state
    };
    console.log(data);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    useEffect(() => {
        dispatch(fetchComment(state.news_id));
    },[dispatch]);

    const onCommentFormSubmit = () => {
        dispatch(createComment(state));
    };

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Paper className={classes.itemMessage}>
                    <h4>Author: {post.title}</h4>
                    <p>Message: {post.content}</p>
                    <p>Date: {post.date}</p>
                    <CardMedia
                        className={classes.cover}
                        image={'http://localhost:8000/uploads/' + post.image}
                    />
                    <IconButton>
                        delete
                    </IconButton>
                </Paper>
                <form onSubmit={onCommentFormSubmit}>
                    <Grid container direction="column" spacing={3}>
                        <Grid item xs>
                            <h2>Comment</h2>
                        </Grid>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                variant="outlined"
                                id="name"
                                label="Name"
                                name="name"
                                value={state.name}
                                onChange={inputChangeHandler}
                            />
                        </Grid>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                multiline
                                rows={3}
                                variant="outlined"
                                id="comment"
                                label="Comment"
                                name="comment"
                                value={state.comment}
                                onChange={inputChangeHandler}
                                required
                            />
                        </Grid>
                        <Grid item xs>
                            <Button type="submit" color="primary" variant="contained">
                                Add
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        </>
    );
};

export default PostInfo;