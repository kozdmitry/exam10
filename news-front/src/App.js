import React from 'react';
import {Switch, Route} from 'react-router-dom';
import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Posts from "./containers/Posts/Posts";
import Container from "@material-ui/core/Container";
import NewPost from "./containers/NewPost/NewPost";
import PostInfo from "./components/PostInfo/PostInfo";

const App = () => (
  <>
    <CssBaseline/>
    <header>
      <AppToolbar/>
    </header>
    <main>
      <Container maxWidth="xl">
        <Switch>
          <Route path="/" exact component={Posts} />
          <Route path="/posts/new" component={NewPost} />
          <Route path="/posts/:id" component={PostInfo} />
        </Switch>
      </Container>
    </main>
  </>
);

export default App;
