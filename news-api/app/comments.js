const express = require('express');
const path = require('path');
const fileDb = require('../fileDb');
const config = require('../config');
const router = express.Router();
const multer = require('multer');

const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    const comment = await fileDb.getItems();
    res.send(comment);
});


router.delete('/:id', async(req, res) => {
    const delComment = await  fileDb.delItem(req.params.id);
    res.send(delComment);
});

router.post("/", upload.single("image"), async (req, res) => {
    const comment = req.body;
    if (comment.comment !== "") {
        if (comment.name === "") {
            comment.name = "Anonymous";
        }

        await fileDb.addItem(comment);
        res.send(comment);
    }
});

module.exports = router;