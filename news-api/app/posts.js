const express = require('express');
const path = require('path');
const fileDb = require('../fileDb');
const config = require('../config');
const router = express.Router();
const multer = require('multer');

const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    const posts = await fileDb.getItems();
    res.send(posts);
});

router.get('/:id', async (req, res) => {
    const post = await fileDb.getItemById(req.params.id);
    res.send(post);
});

router.delete('/:id', async(req, res) => {
    const del = await  fileDb.delItem(req.params.id);
    res.send(del);
});

router.post("/", upload.single("image"), async (req, res) => {
    const post = req.body;
    if (post.content !== "") {
        if (post.title === "") {
            post.author = "Anonymous";
        }

        if (req.file) {
            post.image = req.file.filename;
        }

        await fileDb.addItem(post);
        res.send(post);
    }
});

module.exports = router;