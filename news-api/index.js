const express = require('express');
const fileDb = require('./fileDb');
const posts = require('./app/posts');
const comments = require('./app/comments');
const cors = require("cors");

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/posts', posts);
app.use('/comments', comments);

const port = 8000;


const run = async () => {
    await fileDb.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(console.error)



